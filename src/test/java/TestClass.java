import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import spark.Spark;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

public class TestClass {
    private
    @BeforeAll
    static void setUp() throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, IOException {
        Main.main(null);
        Spark.awaitInitialization();
    }

    @AfterAll
    static void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        Spark.awaitStop();
    }

    @Test
    void testGeneratingInfo() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {

        String response = getStringResponse("getPoints");
        JSONObject respObj = new JSONObject(response);
        JSONArray jsonArray = respObj.getJSONArray("points");

        Signature ecdsaVerify = Signature.getInstance(respObj.getString("algorithm"));
        String pubKey = respObj.getString("publicKey");
        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(pubKey));
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
        String fakeSign = "AAAaaaAAAaaa111AAAaaaAAAaaa111111AAAaaaAAAAAAAAAAAAAAAaaaaaaa111AAAAAAAAAAAAAAAAAaaaaaAAAAAAaaa1";
//        System.out.println(respObj);
//        System.out.println(jsonArray);
//        ecdsaVerify.initVerify(publicKey);
        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject object = jsonArray.getJSONObject(i);
            String trueSignature = object.getString("signature");
            JSONObject pointJson = object.getJSONObject("point");
            Point point = new Point(pointJson.getInt("x"),
                    pointJson.getInt("y"));
            if(trueSignature.equals(fakeSign)){
                assertFalse(checkIfTrueSender(ecdsaVerify, publicKey, point, trueSignature));
            }else {
                assertTrue(checkIfTrueSender(ecdsaVerify, publicKey, point, trueSignature));
            }
        }
    }

    private static boolean checkIfTrueSender(Signature sign, PublicKey pKey, Point point, String signature) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
        try{
            sign.initVerify(pKey);
            sign.update(point.getString().getBytes(StandardCharsets.UTF_8));
            return sign.verify(Base64.getDecoder().decode(signature));
        }catch (Exception e){
//            System.out.println(signature);
//            System.out.println(point.toString());
            return false;
        }
    }

    private static String getStringResponse(String route) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String formatRoute = String.format("http://localhost:4567/%s", route);

        HttpGet httpGet = new HttpGet(formatRoute);
        CloseableHttpResponse response = httpClient.execute(httpGet);

        int statusCode = response.getStatusLine().getStatusCode();
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        if(statusCode == 200)
            return result.toString();
        return null;
    }
}
