import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static spark.Spark.*;

public class Main {
    private static String SPEC = "secp256k1";
    private static String ALGO = "SHA256withECDSA";
    private static PublicKey publicKey;
    private static PrivateKey privateKey;
    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        port(getHerokuAssignedPort());
        get("/getPoints", (req, res) -> getPoints());
    }

    private static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null)
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        return 4567;
    }

    private static void generateKeys() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        Security.insertProviderAt(new BouncyCastleProvider(), 1);
        ECGenParameterSpec ecSpec = new ECGenParameterSpec(SPEC);
        KeyPairGenerator g = KeyPairGenerator.getInstance("EC");
        g.initialize(ecSpec, new SecureRandom());
        KeyPair keypair = g.generateKeyPair();
        publicKey = keypair.getPublic();
        privateKey = keypair.getPrivate();
    }

    private static JSONObject getPoints() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        if(null == publicKey && null == privateKey)
            generateKeys();
        JSONObject object = new JSONObject();
        String falseSignature = "AAAaaaAAAaaa111AAAaaaAAAaaa111111AAAaaaAAAAAAAAAAAAAAAaaaaaaa111AAAAAAAAAAAAAAAAAaaaaaAAAAAAaaa1";
        try {
            Signature ecdsaSign = Signature.getInstance(ALGO);

            String pub = Base64.getEncoder().encodeToString(publicKey.getEncoded());
            List<Point> points = generatePointsList(10);
            JSONArray array = new JSONArray();
            JSONArray finalArray = array;
            points.forEach(
                    i -> {
                        try {
                            ecdsaSign.initSign(privateKey);
                            JSONObject obj = new JSONObject();
                            ecdsaSign.update(i.getString().getBytes(StandardCharsets.UTF_8));
                            byte[] signature = ecdsaSign.sign();
                            String sig = Base64.getEncoder().encodeToString(signature);
                            obj.put("signature", sig);
                            obj.put("point", i.toJson());
                            finalArray.put(obj);
                        } catch (SignatureException | InvalidKeyException e) {
                            e.printStackTrace();
                        }
                    }
            );
            List<Point> falsePoints = generatePointsList(5);
            falsePoints.forEach(
                    i -> {
                        JSONObject obj = new JSONObject();
                        obj.put("signature", falseSignature);
                        obj.put("point", i.toJson());
                        finalArray.put(obj);
                    }
            );
            object.put("publicKey", pub);
            object.put("algorithm", ALGO);
            array = shuffleJsonArray(finalArray);
            object.put("points", array);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        return object;
    }

    private static List<Point> generatePointsList(int size){
        List<Integer> xCoords = new Random().ints(size, 0, 282)
                .boxed().collect(Collectors.toList());
        List<Integer> yCoords = new Random().ints(size, 0, 228)
                .boxed().collect(Collectors.toList());
        return IntStream.range(0, xCoords.size())
                .mapToObj(i -> new Point(xCoords.get(i), yCoords.get(i)))
                .collect(Collectors.toList());
    }

    private static JSONArray shuffleJsonArray(JSONArray array){
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            // Simple swap
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }
}
